import styles from "./footer.module.css";
import responsive from "../../utilities/responsive.module.css";
export default function Footer() {
  return (
    <footer id={styles["footer"]}>
      <div className={responsive.wrapper}>
        <p>&copy; {new Date().getFullYear()} Fake Company</p>
      </div>
    </footer>
  );
}
