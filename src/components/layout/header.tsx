import Link from "next/link";
import Image from "next/image";
import styles from "./header.module.css";
import responsive from "../../utilities/responsive.module.css";
import popcorn from "./popcorn.webp";

export default function Header() {
  return (
    <header id={styles["header"]}>
      <nav id={styles["main-menu"]} className={responsive.wrapper}>
        <ul className={styles["menu"]}>
          <li>
            <Link href={"/"} className={styles.logo}>
              <Image
                src={popcorn}
                alt={"Movie Stream Popcorn Logo"}
                height={45}
              />
              <span className={styles["logo-text"]}>Movie Stream</span>
            </Link>
          </li>
          <li className={styles["menu-link"]}>
            <Link href={"/movies"}>Movies</Link>
          </li>
        </ul>
        <ul className={styles["menu"]}>
          <li className={styles["menu-link"]}>
            <Link href={"/login"}>Login</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}
