import styles from "./page.module.css";
import responsive from "../utilities/responsive.module.css";

export default function Home() {
  return (
    <section id={styles["home"]} className={`${responsive.wrapper}`}>
      <div className={styles["page-content"]}>
        <h1 className={styles["page-title"]}>Stream and Chill</h1>
        <p className={styles["page-subtitle"]}>
          Search and watch movie trailers from here
        </p>
        <input
          className={styles["search-input"]}
          type="search"
          placeholder={"Get your favorite movie !"}
        />
      </div>
    </section>
  );
}
