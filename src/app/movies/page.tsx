import { handleRequest } from "@/libraries/next-tmdb-api/requestHandler";
import { Movie } from "@/libraries/next-tmdb-api/types";
import Image from "next/image";
import { getImagePath } from "@/libraries/next-tmdb-api/imageHandler";
import styles from "./page.module.css";
import responsive from "../../utilities/responsive.module.css";
import Link from "next/link";

async function getCurrentMovies(): Promise<any> {
  const res = await handleRequest({ endpoint: "movie/now_playing" });
  return res.results;
}

export default async function Movie() {
  const currentMovies = await getCurrentMovies();

  return (
    <section className={`${responsive.wrapper} mb-5 `}>
      <h1 className={`title mb-4 ${styles["main-title"]}`}>
        Currently in Theaters
      </h1>
      <div id={styles["movie-list"]}>
        {currentMovies.map((movie: Movie) => {
          return (
            <a
              className={styles["card"]}
              href={`https://www.themoviedb.org/movie/${movie.id}`}
              key={`movie_card_${movie.id}`}
              target={"_blank"}
              rel="noreferrer noopener"
            >
              <div className={styles["image-container"]}>
                <Image
                  src={getImagePath(movie.poster_path, "w500")}
                  alt={`${movie.title} poster`}
                  width={500}
                  height={750}
                  className={styles["cover"]}
                />
                <div className={styles["cover-hover"]}>
                  <span>More</span>
                </div>
              </div>
              <div className={styles["details"]}>
                <h2 className={styles["title"]}>{movie.title}</h2>
                <span className={styles["release-date"]}>
                  {movie.release_date}
                </span>
              </div>
            </a>
          );
        })}
      </div>
    </section>
  );
}
