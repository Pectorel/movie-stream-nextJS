const base = "https://api.themoviedb.org/";

export interface TmdbRequest {
  endpoint: string;
  version?: string | number;
  method?: "get" | "post" | "delete";
}

/**
 * Setting up all TmdbRequest interface dafault values
 */
const defaults: Pick<TmdbRequest, "version" | "method"> = {
  version: "3",
  method: "get",
};

/**
 * Setting up all default values for TmdbRequest variables
 * @param req
 */
function setDefault(req: TmdbRequest): TmdbRequest {
  return {
    ...defaults,
    ...req,
  };
}

/**
 * Construct the API url with given infos
 * @param req
 */
export function constructUrl(req: TmdbRequest): string {
  req = setDefault(req);
  return `${base}${req.version}/${req.endpoint}`;
}

/**
 *
 * Handle API Requests
 *
 * @param req
 */
export async function handleRequest(req: TmdbRequest): Promise<any> {
  req = setDefault(req);
  const url = constructUrl(req);

  const res = await fetch(url, {
    method: req.method,
    headers: { Authorization: `Bearer ${process.env.TMDB_BEARER}` },
  });

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }

  return await res.json();
}
