const base_url = "https://image.tmdb.org/t/p/";

type BackdropSize = "w300" | "w780" | "w1280" | "original";
type LogoSize = "w45" | "w92" | "w154" | "w185" | "w300" | "w500" | "original";
type PosterSize =
  | "w92"
  | "w154"
  | "w185"
  | "w342"
  | "w500"
  | "w780"
  | "original";
type ProfileSize = "w45" | "w185" | "h632" | "original";
type StillSize = "w92" | "w185" | "w300" | "original";

export function getImagePath(
  path: string,
  size: BackdropSize | LogoSize | PosterSize | ProfileSize | StillSize,
): string {
  return `${base_url}${size}${path}`;
}
